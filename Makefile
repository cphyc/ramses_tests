SUBDIRS:=$(shell find . -type d -name 'test_*')

.PHONY: $(SUBDIRS)

all: setup $(SUBDIRS)

setup:
	mkdir -p bin
	rm -f bin/ramses3d
	ln -s $(RAMSES_ROOT)/bin/ramses_patched3d bin/ramses3d


$(SUBDIRS):
	@echo ">>>>>>>> Descending into $@ <<<<<<<<"
	@$(MAKE) -C $@ RAMSES="$(shell pwd)/bin/ramses3d"
