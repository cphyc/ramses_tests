#!/usr/bin/env bash

for d in $(find . -name 'output_*' -type d); do
    echo "$d"
    for p in amr hydro; do
	file=$(ls $d/$p*out*);
	echo -n "| "
	md5sum $file
    done
done
