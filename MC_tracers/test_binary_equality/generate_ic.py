import numpy as np
from itertools import product
from tqdm import tqdm

n = 32
nin = 1
nout = 1

def check(x, y, z):
    # Slab
    return True

with open('ic_tracers', 'w') as f:
    dx = 1./(n)
    for ix, iy, iz in tqdm(product(*[range(n)]*3), total=n**3):
        x, y, z = (ix+0.5)*dx, (iy+0.5)*dx, (iz+0.5)*dx
        if check(x, y, z):
            [f.write('%15.14f %15.14f %15.14f %15.14f %15.14f %15.14f %15.14f\n'
                     % (x, y, z, 0, 0, 0, 1.0))
             for i in range(nin)]
        else:
            [f.write('%15.14f %15.14f %15.14f %15.14f %15.14f %15.14f %15.14f\n'
                     % (x, y, z, 0, 0, 0, 1.0))
             for i in range(nout)]
