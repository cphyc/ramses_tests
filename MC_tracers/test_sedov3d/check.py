#!/usr/bin/env python
import numpy as np
import yt
import matplotlib.pyplot as plt
yt.enable_parallelism()
yt.funcs.mylog.setLevel(11)


dss = yt.load('output_*/info*.txt',
              extra_particle_fields=[('particle_family', 'I')])

gas_factor = tracer_factor = None
ds = yt.load('output_00001/info_00001.txt')
rp = yt.create_profile(ds.all_data(), 'radius', ['all_density', 'density'],
                       deposition='cic')

gas_factor = rp['density'][0]
tracer_factor = rp['all_density'][0]

for ds in dss.piter():
    rp = yt.create_profile(ds.all_data(), 'radius', ['all_density', 'density'],
                           deposition='cic')

    r = rp.x.value

    gas_density = rp['density'] / gas_factor
    tracer_density = rp['all_density'] / tracer_factor

    plt.cla()
    plt.scatter(r, gas_density, label='Gas')
    plt.scatter(r, tracer_density, label='MC Tracers')
    plt.xlabel('Radius')
    plt.ylabel('Density')

    plt.legend()
    plt.ylim(ymin=1e-2)
    plt.yscale('log')
    plt.xlim(0, 0.5)
    plt.savefig('profile_%s.png' % str(ds))

    p = yt.SlicePlot(ds, 'z', ['density', 'all_density'])
    p.annotate_grids()
    p.zoom(2)
    p.set_log('density', True)
    p.set_zlim('density', zmin=None, zmax='max', dynamic_range=1e4)
    p.set_zlim('all_density', zmin=None, zmax='max', dynamic_range=1e4)
    p.annotate_timestamp()
    p.save()


