import numpy as np
from itertools import product
from tqdm import tqdm

npart = 100000
def check(x, y, z):
    # Slab
    return True

with open('ic_tracers', 'w') as f:
    for i in range(npart):
        x, y, z = np.array([.5, .5, .5]) + (np.random.random(3)-0.5)/1000
        f.write('%15.14f %15.14f %15.14f %15.14f %15.14f %15.14f %15.14f\n'
                % (x, y, z, 0, 0, 0, 1.0))
