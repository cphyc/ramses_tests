import yt
from glob import glob
import numpy as np
try:
    from numba import jit
except:
    print('Jit not found, falling back')
    jit = lambda f: f
import sys

feedback_kind = sys.argv[1]

yt.enable_parallelism()
yt.funcs.mylog.setLevel(50)

ptypes = {
    0: 'DM',
    1: 'star',
    100: 'MC_tracer',
    101: 'star_tracer',
    110: 'old_star_tracer'
}

def gen_filter(pt):
    def loc(pfilter, data):
        if np.isnan(pt):
            return data[(pfilter.filtered_type, "particle_type")] > 101
        else:
            return data[(pfilter.filtered_type, "particle_type")] == pt
    return loc


for pt in ptypes:
    yt.add_particle_filter(ptypes[pt], function=gen_filter(pt),
                           requires=['particle_type'], filtered_type='all')


def released_tracers(pfilter, data):
    return data[(pfilter.filtered_type, "particle_type")] > 101


yt.add_particle_filter('old_star_tracer', function=released_tracers,
                       requires=['particle_type'], filtered_type='all')


def load_filters(ds):
    for pt in ptypes:
        ds.add_particle_filter(ptypes[pt])


@jit
def AinB(A, B):
    res = np.zeros(A.shape[0])
    for i, a in enumerate(A):
        res[i] = a in B

    return res


load_kwa = dict(extra_particle_fields=[('particle_type', 'I')])


def check_attachment():
    outputs = sorted(glob('output_*/info*.txt'))

    # Now let's take a look at their trajectories
    dss = yt.load(outputs[:], **load_kwa)

    all_ok = {}
    for (sto, ds) in dss.piter(storage=all_ok):
        load_filters(ds)
        ad = ds.all_data()

        # Check that particles following stars have a star
        tracer_pos = ad['star_tracer', 'particle_position']
        star_pos = ad['star', 'particle_position']

        ok = AinB(tracer_pos, star_pos) # all([tp in star_pos for tp in tracer_pos])
        ok = all(ok)

        # Check that particles on grid are on grid!
        tracer_pos = ad['MC_tracer', 'particle_position']

        Ngrid = 64

        ok = ok and all([np.all(np.mod(tp * Ngrid, 1) == 0)
                         for tp in tracer_pos])

        if not ok:
            print(ds, 'is not ok!')
            assert ok

        sto.result = ok

    # Flatten result array
    all_ok = [all_ok[k] for k in all_ok]

    assert(all(all_ok))


def check_trajs():
    outputs = sorted(glob('output_*/info*.txt'))

    # Now let's take a look at their trajectories
    dss = yt.load(outputs[:], **load_kwa)
    ds = dss[-1]

    load_filters(ds)

    fields = ['particle_type', 'particle_identifier']
    ids = ds.r['particle_identifier'][ds.r['particle_type'] >= 100]
    trajs = dss.particle_trajectories(
        ids, fields=fields)

    # Keep those trajs that crosses a star
    for tid in ids:
        tid = np.int(tid)
        m = trajs['particle_identifier'] == tid

        w = np.argwhere(trajs['particle_type'][m] == 101)
        if len(w) > 0:
            first_star = w[0, 0]
        else:
            continue

        w = np.argwhere(trajs['particle_type'][m][first_star:] != 101)
        if len(w) > 0:
            first_release = first_star + w[0, 0]
        else:
            continue

        print(first_star, first_release)

    return trajs


def check_mass_conservation():
    # yt.enable_parallelism()
    outputs = sorted(glob('output_*/info*.txt'))[::2]

    dss = yt.load(outputs[:], **load_kwa)

    def get_mass(ds, ptype, mass_unit=1):
        ad = ds.all_data()
        if type(ptype) == list:
            return [(ad.sum('%s_mass' % pt) * mass_unit).in_units('Msun').value
                    for pt in ptype]
        else:
            return (ad.sum('%s_mass' % ptype) * mass_unit).in_units('Msun').value

    ds = dss[1]
    load_filters(ds)
    ad = ds.all_data()
    gas_mass = sum(get_mass(ds, ['star', 'cell']))
    tracer_mass = sum(get_mass(ds, ['MC_tracer', 'star_tracer',
                                    'old_star_tracer']))

    mass_unit = gas_mass / tracer_mass

    storage = {}
    for sto, ds in dss.piter(storage=storage):
        print(ds)
        load_filters(ds)
        res = {}

        res['gas_mass'], res['star_mass'] = get_mass(ds, ['cell', 'star'])
        (res['MC_tracer_mass'],
         res['star_tracer_mass'],
         res['other_tracer_mass']) = get_mass(ds, ['MC_tracer',
                                                   'star_tracer',
                                                   'old_star_tracer'],
                                              mass_unit=mass_unit)

        c = lambda t: ds.r[t, 'particle_ones'].sum().astype(int).value
        res.update({
            'dataset': str(ds),
            'ds_index': int(str(ds).split('_')[1]),
            'time': ds.current_time.in_units('Myr'),
            'MC_tracer': c('MC_tracer'),
            'star_tracer': c('star_tracer'),
            'star': c('star')
        })
        sto.result = res

    if yt.is_root():
        import matplotlib.pyplot as plt
        import pandas as pd

        print(storage)

        df = pd.DataFrame(storage).T
        print(df)

        def g(storage, key, return_key=True):
            u = (yt.units.Msun * 1e11).in_cgs()
            keys = sorted([k for k in storage.keys()])
            values = np.array([storage[k][key] / u for k in keys])

            if return_key:
                return np.array(keys), values
            else:
                return values

        tot_tracer = sum([g(storage, f, return_key=False)
                          for f in ['MC_tracer_mass',
                                    'star_tracer_mass',
                                    'other_tracer_mass']])
        tot_baryons = sum([g(storage, f, return_key=False)
                          for f in ['star_mass', 'gas_mass']])

        plt.figure()
        # Estimate error on star tracer mass (Poisson)
        # x = df.ds_index.astype(int)
        x = df.time.astype(float)
        plt.plot(x, df.star_tracer_mass, ls='--', label='star tracer')
        dy = (1. / np.sqrt(df.star_tracer.astype(float))
              * df.star_tracer_mass).astype(float)
        dy = np.where(np.isfinite(dy), dy, 0)

        y = df['star_tracer_mass'].astype(float)
        plt.fill_between(x, y + dy, y - dy, alpha=0.5)

        plt.plot(x, df.other_tracer_mass, ls='--', label='released tracers')
        plt.plot(x, df.star_mass, ls='--', label='star')

        plt.plot(x, df.star_tracer_mass + df.other_tracer_mass, ls=':')
        # plt.plot(x, df.gas_mass, ls='--', label='gas')

        # plt.plot(tot_tracer, label='tot_tracer')
        # plt.plot(tot_baryons, label='tot_baryons')

        plt.ylabel('M [1e11 Msun]')
        plt.xlabel('time [Myr]')

        plt.yscale('log')

        plt.legend()
        plt.savefig('mass_conservation_%s_feedback.pdf' % feedback_kind)

        plt.close()

        return df


storage = check_mass_conservation()

# if __name__ == '__main__':
#     # check_attachment()
#     # check_trajs()
#     storage = check_mass_conservation()
