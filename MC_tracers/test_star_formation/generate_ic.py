import numpy as np
from itertools import product
from tqdm import tqdm

nparts = 10000

xyz = x, y, z = np.random.rand(3, nparts)
vx, vy, vz = np.zeros_like(xyz)
m = np.zeros_like(x)

data = np.array([x, y, z, vx, vy, vz, m]).T

np.savetxt('ic_tracers', data)

