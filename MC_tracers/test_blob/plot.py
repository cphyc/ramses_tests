import yt
yt.enable_parallelism()
yt.funcs.mylog.setLevel(20)
# yt.toggle_interactivity()
dss = yt.load('output_*/info_*.txt',
              extra_particle_fields=[('particle_family', 'i'),
                                     ('particle_pter', 'i')])

for ds in dss.piter():
    p = yt.ProjectionPlot(ds, 'z', ['all_cic', 'metallicity'],
                          weight_field=('index', 'ones'))
    p.set_zlim(['all_cic', 'metallicity'], zmax='max', zmin=None,
               dynamic_range=50)
    p.set_colorbar_label('metallicity', 'Gas density')
    p.set_colorbar_label('all_cic', 'Tracer density')
    p.annotate_timestamp()
    p.save('plots/', suffix='png')
