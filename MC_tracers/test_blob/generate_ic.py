import numpy as np


dmax = .1**2
vol = 4*np.pi/3*dmax**(3/2)
Ntarget = int(1e5)
N = int(Ntarget / vol)

np.random.seed(16091992)
x, y, z = pos = np.random.rand(3, N)

w = ((x-.5)**2 + (y-.5)**2 + (z-.5)**2) < dmax

print(pos.shape, w.shape)

data = np.concatenate((pos[:, w], pos[:, w]*0, pos[0:1, w]*0)).T

np.savetxt('ic_tracers', data)
